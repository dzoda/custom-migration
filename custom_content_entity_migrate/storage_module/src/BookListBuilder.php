<?php

namespace Drupal\storage_module;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/**
 * Defines a class to build a listing of Book entities.
 *
 * @ingroup storage_module
 */
class BookListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['image'] = $this->t('Image');
    $header['id'] = $this->t('Book ID');
    $header['name'] = $this->t('Name');
    $header['isbn'] = $this->t('ISBN');
    $header['price'] = $this->t('Price');
    $header['notes'] = $this->t('Notes');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\storage_module\Entity\Book */
    $image = $entity->get('image')->getValue();
    $image_id = (!empty($image[0]['target_id'])) ? $image[0]["target_id"] : '';
    $file = File::load($image_id);
    $image_path = $file->getFileUri();
    $url = ImageStyle::load('thumbnail')->buildUrl($image_path);


    $row['image'] = $url;
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.book.edit_form',
      ['book' => $entity->id()]
    );
    $row['isbn'] = $entity->getIsbn();
    $row['price'] = $entity->getPrice();
    $row['notes'] = $entity->getNotes();
    return $row + parent::buildRow($entity);
  }

}
