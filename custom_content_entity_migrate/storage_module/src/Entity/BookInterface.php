<?php

namespace Drupal\storage_module\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Book entities.
 *
 * @ingroup storage_module
 */
interface BookInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Book name.
   *
   * @return string
   *   Name of the Book.
   */
  public function getName();

  /**
   * Sets the Book name.
   *
   * @param string $name
   *   The Book name.
   *
   * @return \Drupal\storage_module\Entity\BookInterface
   *   The called Book entity.
   */
  public function setName($name);

  /**
   * Gets the Book creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Book.
   */
  public function getCreatedTime();

  /**
   * Sets the Book creation timestamp.
   *
   * @param int $timestamp
   *   The Book creation timestamp.
   *
   * @return \Drupal\storage_module\Entity\BookInterface
   *   The called Book entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Book published status indicator.
   *
   * Unpublished Book are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Book is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Book.
   *
   * @param bool $published
   *   TRUE to set this Book to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\storage_module\Entity\BookInterface
   *   The called Book entity.
   */
  public function setPublished($published);

  /**
   * Gets the Book revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Book revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\storage_module\Entity\BookInterface
   *   The called Book entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Book revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Book revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\storage_module\Entity\BookInterface
   *   The called Book entity.
   */
  public function setRevisionUserId($uid);

}
