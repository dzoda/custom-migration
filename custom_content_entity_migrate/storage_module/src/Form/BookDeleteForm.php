<?php

namespace Drupal\storage_module\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Book entities.
 *
 * @ingroup storage_module
 */
class BookDeleteForm extends ContentEntityDeleteForm {


}
